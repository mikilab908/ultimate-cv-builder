# Ultimate CV Builder

This project represents an online CV builder where the user can choose four different cv categories. In addition, it has a step-by-step guideline for building a good LinkedIn and Laika profile/portfolio. Built with: React, Bootstrap, HTML, CSS.