import './App.css';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';
import ChooseCv from './components/ChooseCv';
import Homepage from './components/Homepage';
import Popup from './components/Popup';
import CvWrapper from './components/CvWrapper';

function App() {
  return (
    <div className='App'>
      <Router>
        <Redirect to='/' />
        <Switch>
          <Route exact path='/' component={Homepage} />
          <Route path='/choosecv' component={ChooseCv} />
          <Route path='/cvwrapper/:content' component={CvWrapper} />
          <Route path='/popup' component={Popup} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
