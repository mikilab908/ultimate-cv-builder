import React from 'react';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import '../assets/css/ChooseCv.css';
import FooterLinks from './FooterLinks';
import { categories } from '../data';
import { useGlobalContext } from '../context';

function ChooseCv() {
  const { chooseCvRoute } = useGlobalContext();

  return (
    <>
      <div className='container-fluid homepage-bg'>
        <div className='row boxes' id='first-row'>
          <div className='col-md-5  upper-text'>
            <div className='h1'>Choose your category</div>
          </div>
        </div>

        <div className='row boxes'>
          {categories.map((category) => {
            const { title, id, route } = category;

            return (
              <div key={id} className='col box-wrap'>
                <div className='box'>
                  <div className='h3'>{title}</div>
                  <div className='category-btn'>
                    <Link
                      className='btn'
                      to={`/cvwrapper/${route}`}
                      onClick={() => chooseCvRoute(route)}
                    >
                      CHOOSE
                    </Link>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
        <FooterLinks />
      </div>
    </>
  );
}

export default ChooseCv;
