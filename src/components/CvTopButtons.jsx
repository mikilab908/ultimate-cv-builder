import React from 'react';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import '../assets/css/CvWrapper.css';
import { useGlobalContext } from '../context';

function CvTopButtons() {
  const {
    isCvChoosen,
    handleEditMode,
    isActiveClass,
    handleActiveClass,
    targetName,
    location,
    endLocation,
  } = useGlobalContext();

  let initialPath = location.split('/')[1];

  return (
    <div className='row'>
      <div className='col-md-11 top-buttons'>
        <div
          className={targetName === 'CV' || isActiveClass ? 'active cv' : 'cv'}
          id='cv'
        >
          <Link
            className='btn'
            to={`/${initialPath}/${isCvChoosen}`}
            onClick={handleActiveClass}
          >
            CV
          </Link>
        </div>
        <div className={targetName === 'LINKEDIN' ? 'active cv' : 'cv'}>
          <Link
            className='btn'
            to={`/${initialPath}/linked`}
            onClick={handleActiveClass}
          >
            LINKEDIN
          </Link>
        </div>
        <div className={targetName === 'WEARELAIKA.COM' ? 'active cv' : 'cv'}>
          <Link
            className='btn'
            to={`/${initialPath}/laika`}
            onClick={handleActiveClass}
          >
            WEARELAIKA.COM
          </Link>
        </div>
      </div>

      <div className='col-md-1 top-buttons'>
        {isCvChoosen === endLocation && (
          <div className='edit'>
            <button className='btn' onClick={handleEditMode}>
              EDIT
            </button>
          </div>
        )}
      </div>
    </div>
  );
}

export default CvTopButtons;
