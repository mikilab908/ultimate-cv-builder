import React, { useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../assets/css/CvWrapper.css';
import FooterLinks from './FooterLinks';
import CvTopButtons from './CvTopButtons';
import RightSideTextArea from './RightSideTextArea';
import MainCvShowcase from './MainCvShowcase';
import Linked from './Linked';
import Laika from './Laika';
import { useGlobalContext } from '../context';

function CvWrapper(props) {
  const { handleLocation, handleEndLocation } = useGlobalContext();

  useEffect(() => {
    handleLocation(props.location);
    handleEndLocation(props.match.params.content);
  });

  let component;

  switch (props.match.params.content) {
    case 'cv':
      component = <MainCvShowcase />;
      break;

    case 'linked':
      component = <Linked />;
      break;

    case 'laika':
      component = <Laika />;
      break;

    default:
      component = <MainCvShowcase />;
      break;
  }
  return (
    <div className='container-fluid'>
      <div className='row cv-wrapper-bg'>
        <div className='col-md-8' id='left-side'>
          <div className='left-side'>
            <CvTopButtons />
            {component}
          </div>
        </div>
        <RightSideTextArea />
      </div>
      <FooterLinks />
    </div>
  );
}

export default CvWrapper;
