import '../assets/css/Design.css';
import designResImg from '../assets/images/template/Graphic Design Resume.png';
import { useGlobalContext } from '../context';
import EditDesign from './editCvComponents/EditDesign';

const Design = () => {
  const { isEditMode, section, handleTips } = useGlobalContext();

  return (
    <>
      {isEditMode && (
        <div>
          <EditDesign />
        </div>
      )}

      {!isEditMode && (
        <div className='design-res'>
          <img
            className='d-block w-100'
            src={designResImg}
            alt='science resume'
          />
          <div
            className='mainInfo'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='intro'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='previousJob'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='education'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='skills'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='practicalPart'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
        </div>
      )}
    </>
  );
};

export default Design;
