import '../assets/css/Development.css';
import developResImg from '../assets/images/template/Web Development Resume.png';
import { useGlobalContext } from '../context';
import EditDevelopment from './editCvComponents/EditDevelopment';

const Development = () => {
  const { isEditMode, section, handleTips } = useGlobalContext();

  return (
    <>
      {isEditMode && (
        <div>
          <EditDevelopment />
        </div>
      )}

      {!isEditMode && (
        <div className='development-res'>
          <img
            className='d-block w-100'
            src={developResImg}
            alt='development resume'
          />

          <div
            className='photo'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='mainInfo'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='intro'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='previousJob'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='practicalPart'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='skills'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='education'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
        </div>
      )}
    </>
  );
};

export default Development;
