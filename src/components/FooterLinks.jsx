import React from 'react';
import '../assets/css/FooterLinks.css';
import 'bootstrap/dist/css/bootstrap.css';

function FooterLinks() {
  return (
    <>
      <div className='row'>
        <div className='col-md-12 footer-banner'>
          <div className='left-banner'>
            <h5>Do you want to learn hands-on digital skills?</h5>
            <div className='banner-btn'>
              <a
                href='https://brainster.co/'
                target='_blank'
                rel='noreferrer'
                className='btn'
              >
                Visit Brainster
              </a>
            </div>
          </div>
          <div className='left-banner right-banner'>
            <h5>Do you want to recieve job offers by IT Companies?</h5>
            <div className='banner-btn'>
              <a
                href='https://www.wearelaika.com/'
                target='_blank'
                rel='noreferrer'
                className='btn'
              >
                Visit Laika
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default FooterLinks;
