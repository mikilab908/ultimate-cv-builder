import React from 'react';
import { Link } from 'react-router-dom';
import '../assets/css/Homepage.css';
import 'bootstrap/dist/css/bootstrap.css';
import homeImg from '../assets/images/BackgroundFirstPage.png';

function Homepage() {
  return (
    <>
      <div className='container-fluid homepage-bg'>
        <div className='row'>
          <div className='col-md-5 offset-md-1 homepage-txt'>
            <div className='home-text'>
              <div className='h1'>
                The Ultimate <br />
                CV & Portfolio Check - List
              </div>
              <p>
                Are you a Web Developer, Data Scientist, Digital Marketer or a
                Designer? Have you CV and portfolio in check and create a 5-star
                representation of you skills with this guide.
              </p>
              <Link className='btn' to='/choosecv'>
                STEP INSIDE
              </Link>
            </div>
          </div>
          <div className='col-md-5 homepage-img'>
            <img src={homeImg} alt='home-img' />
          </div>
        </div>
        <div className='row'>
          <div className='col-md-12 footer'>
            <div className='footText'>
              <p>
                Created with &lt;3 by the
                <span>
                  <a
                    target='_blank'
                    rel='noreferrer'
                    href='https://codepreneurs.brainster.co/'
                  >
                    {' '}
                    Brainster Coding Academy{' '}
                  </a>
                </span>
                students and
                <span>
                  <a
                    target='_blank'
                    rel='noreferrer'
                    href='https://www.wearelaika.com/'
                  >
                    {' '}
                    wearelaika.com
                  </a>
                </span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Homepage;
