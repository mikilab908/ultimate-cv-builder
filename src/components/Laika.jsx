import React, { useEffect } from 'react';
import '../assets/css/Laika.css';
import { useGlobalContext } from '../context';

const Laika = () => {
  const { endLocation, handleTips, handleLaika } = useGlobalContext();

  useEffect(() => {
    handleLaika(true);
  });

  return (
    <div className='col-md-10 imageCv'>
      <div className='laika'>
        <div className='email-div'>
          <img
            className='d-block w-100'
            src={`/laika/laika1.png`}
            alt='laika1'
          />
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='email'
          ></div>
        </div>
        <div className='social-media-div'>
          <img
            className='d-block w-100'
            src={`/laika/laika2.png`}
            alt='laika2'
          />
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='socialMedia'
          ></div>
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='whyLaika'
          ></div>
        </div>
        <div className='industry-div'>
          <img
            className='d-block w-100'
            src={`/laika/laika3.png`}
            alt='laika3'
          />
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='industry'
          ></div>
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='expertise'
          ></div>
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='technologies'
          ></div>
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='experience'
          ></div>
        </div>
        <div className='education-div'>
          <img
            className='d-block w-100'
            src={`/laika/laika4.png`}
            alt='laika4'
          />
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='education'
          ></div>
        </div>
        <div className='job-type-div'>
          <img
            className='d-block w-100'
            src={`/laika/laika5.png`}
            alt='laika5'
          />
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='locations'
          ></div>
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='jobType'
          ></div>
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='jobTitle'
          ></div>
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='opportunity'
          ></div>
        </div>
        <div className='experience-level-div'>
          <img
            className='d-block w-100'
            src={`/laika/laika6.png`}
            alt='laika6'
          />
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='experienceLevel'
          ></div>
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='salary'
          ></div>
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='jobPlan'
          ></div>
        </div>
      </div>
    </div>
  );
};

export default Laika;
