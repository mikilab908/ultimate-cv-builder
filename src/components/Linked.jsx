import React, { useEffect } from 'react';
import '../assets/css/Linked.css';
import { useGlobalContext } from '../context';

const Linked = () => {
  const { endLocation, handleTips, handleLinkedIn } = useGlobalContext();

  useEffect(() => {
    handleLinkedIn(true);
  });

  return (
    <div className='col-md-10 imageCv'>
      <div className='linked'>
        <div className='about-div'>
          <img src={`/linked/bill1.png`} alt='bill1' />
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='title'
          ></div>
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='about'
          ></div>
        </div>
        <div className='acivity-div'>
          <img src={`/linked/bill2.png`} alt='bill2' />
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='activity'
          ></div>
        </div>
        <div className='experience-div'>
          <img src={`/linked/bill3.png`} alt='bill3' />
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='experience'
          ></div>
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='education'
          ></div>
        </div>
        <div className='interests-div'>
          <img src={`/linked/bill4.png`} alt='bill4' />
          <div
            onMouseEnter={(e) => handleTips(e.target.className, endLocation)}
            className='interests'
          ></div>
        </div>
      </div>
    </div>
  );
};

export default Linked;
