import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../assets/css/CvWrapper.css';
import Development from './Development';
import Science from './Science';
import Marketing from './Marketing';
import Design from './Design';
import { useGlobalContext } from '../context';
import { Link } from 'react-router-dom';
import { Modal, Button } from 'react-bootstrap';

function MainCvShowcase() {
  const {
    isEditMode,
    location,
    handleDownloadPdf,
    isLinkedChecked,
    isLaikaChecked,
    PdfDownloadCheck,
    fieldsFilled,
    modal,
    handleClose,
  } = useGlobalContext();

  let component;

  switch (location) {
    case '/cvwrapper/development':
      component = <Development />;
      break;

    case '/cvwrapper/science':
      component = <Science />;
      break;

    case '/cvwrapper/marketing':
      component = <Marketing />;
      break;

    case '/cvwrapper/design':
      component = <Design />;
      break;

    default:
      component = <Development />;
      break;
  }

  return (
    <div className='row'>
      <div className='col-md-10 imageCv' id='current-component'>
        {component}
      </div>
      <div className='col-md-2 download-wrap'>
        <div className='download'>
          {isEditMode && (
            <Link
              to={
                fieldsFilled >= 6 && isLinkedChecked && isLaikaChecked
                  ? '/popup'
                  : '#'
              }
              className='btn'
              onMouseEnter={PdfDownloadCheck}
              onClick={handleDownloadPdf}
              data-toggle='modal'
              data-target='#downloadModal'
            >
              DOWNLOAD
            </Link>
          )}
        </div>
      </div>
      <div className='col'>
        <Modal show={modal} onHide={handleClose}>
          <Modal.Header></Modal.Header>
          <Modal.Body>
            {fieldsFilled < 6
              ? 'Please fill more sections in the Cv builder, at least 4 sections'
              : !isLinkedChecked && !isLaikaChecked
              ? 'Please first check LinkedIn and WeAreLaika guideline for perfect profile/portfolio'
              : !isLinkedChecked
              ? 'Please check LinkedIn step-by-step guideline'
              : 'Please check WeAreLaika step-by-step guideline'}
          </Modal.Body>
          <Modal.Footer>
            <Button variant='secondary modal-btn' onClick={handleClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    </div>
  );
}

export default MainCvShowcase;
