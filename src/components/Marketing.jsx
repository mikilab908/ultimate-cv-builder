import '../assets/css/Marketing.css';
import marketResImg from '../assets/images/template/Digital Marketing Resume.png';
import { useGlobalContext } from '../context';
import EditMarketing from '../components/editCvComponents/EditMarketing';
const Marketing = () => {
  const { isEditMode, section, handleTips } = useGlobalContext();

  return (
    <>
      {isEditMode && (
        <div>
          <EditMarketing />
        </div>
      )}

      {!isEditMode && (
        <div className='marketing-res'>
          <img
            className='d-block w-100'
            src={marketResImg}
            alt='marketing resume'
          />

          <div
            className='photo'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='mainInfo'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='previousJob'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='education'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='practicalPart'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='skills'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='achievements'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='languages'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
        </div>
      )}
    </>
  );
};

export default Marketing;
