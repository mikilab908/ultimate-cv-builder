import React from 'react';
import '../assets/css/Popup.css';

const Popup = () => {
  return (
    <div className='container-fluid popup'>
      <div className='popup-text'>
        <h1>
          Did you finish all <br />
          three? Way to go!
        </h1>
        <p>
          Good luck on your next job! If <br />
          you need help, counseling or <br />
          just want to leave a suggestion, <br />
          contact us at
        </p>
        <span>
          {' '}
          <a href='mailto:hello@wearelaika.com'>hello@wearelaika.com</a>
        </span>
      </div>

      <div className='row'>
        <div className='col-md-12 footer'>
          <div className='footText'>
            <p>
              Created with &lt;3 by the
              <span>
                <a
                  target='_blank'
                  rel='noreferrer'
                  href='https://codepreneurs.brainster.co/'
                >
                  {' '}
                  Brainster Coding Academy{' '}
                </a>
              </span>
              students and
              <span>
                <a
                  target='_blank'
                  rel='noreferrer'
                  href='https://www.wearelaika.com/'
                >
                  {' '}
                  wearelaika.com
                </a>
              </span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Popup;
