import React from 'react';
import { useGlobalContext } from '../context';

function RightSideTextArea() {
  const { displayTips } = useGlobalContext();

  return (
    <div className='col-md-3 offset-md-1 right-side'>
      <h4>{displayTips}</h4>
    </div>
  );
}

export default RightSideTextArea;
