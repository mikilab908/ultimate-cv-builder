import '../assets/css/Science.css';
import scienceResImg from '../assets/images/template/Data Scientist Resume.png';
import { useGlobalContext } from '../context';
import EditScience from './editCvComponents/EditScience';

const Science = () => {
  const { isEditMode, section, handleTips } = useGlobalContext();

  return (
    <>
      {isEditMode && (
        <div>
          <EditScience />
        </div>
      )}
      {!isEditMode && (
        <div className='science-res'>
          <img
            className='d-block w-100'
            src={scienceResImg}
            alt='science resume'
          />

          <div
            className='photo'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='previousJob'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='education'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='achievements'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='skills'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='mainInfo'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='languages'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='informalEducation'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
          <div
            className='practicalPart'
            onClick={(e) => handleTips(e.target.className, section)}
          ></div>
        </div>
      )}
    </>
  );
};

export default Science;
