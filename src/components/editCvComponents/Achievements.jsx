import { React, useState } from 'react';
import TextareaAutosize from 'react-textarea-autosize';
import { useGlobalContext } from '../../context';
import { v4 as uuid } from 'uuid';
import AddButton from './AddButton';
import DeleteButton from './DeleteButton';

function Achievements() {
  const {
    achievements,
    handleAchievements,
    deleteAchievement,
    section,
    handleTips,
    editAchievements,
  } = useGlobalContext();
  const [focused, setFocused] = useState(true);

  return (
    <div className='achievement'>
      <h5>ACHIEVEMENTS & CERTIFICATES</h5>

      <AddButton
        name='addButton'
        function={() =>
          handleAchievements({
            id: uuid(),
            achievement: '',
            description: '',
          })
        }
        isIgnored={true}
      />

      {achievements &&
        achievements.map((el, idx) => (
          <div className='form-wrapper' id={el.id} key={idx}>
            <ul className='info-achievement'>
              {focused && (
                <DeleteButton
                  name='deleteButton'
                  function={() => deleteAchievement(el.id)}
                  isIgnored={true}
                />
              )}

              <li className='studied'>
                <TextareaAutosize
                  name='achievements'
                  className='courses'
                  placeholder='Achievements/Certificates Name'
                  type='text'
                  onFocus={() => setFocused(true)}
                  onBlur={() => setFocused(false)}
                  onClick={(e) => handleTips(e.target.name, section)}
                  value={el.achievements}
                  onChange={(e) => editAchievements(e, el.id, e.target.name)}
                />
              </li>
              <li>
                <TextareaAutosize
                  name='description'
                  className='projects'
                  placeholder='Course/Thesis/Project Description'
                  type='text'
                  onFocus={() => setFocused(true)}
                  onBlur={() => setFocused(false)}
                  value={el.description}
                  onChange={(e) => editAchievements(e, el.id, e.target.name)}
                />
              </li>
            </ul>
          </div>
        ))}
    </div>
  );
}

export default Achievements;
