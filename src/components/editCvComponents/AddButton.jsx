import React from 'react';
import { useGlobalContext } from '../../context';

function AddButton(props) {
  const { icons } = useGlobalContext();

  return (
    <div
      className='addButton'
      onClick={props.function}
      data-html2canvas-ignore={props.isIgnored}
    >
      {icons[props.name]}
    </div>
  );
}

export default AddButton;
