import React from 'react';
import TextareaAutosize from 'react-textarea-autosize';
import { useGlobalContext } from '../../context';

function BasicInfo() {
  const {
    section,
    handleTips,
    basicInfo,
    handleBasicInfoInput,
  } = useGlobalContext();

  return (
    <div className='basic-info'>
      <div className='side-box'>
        <div className='box'></div>
      </div>
      <div className='basic-info-form'>
        <h2 className='name'>
          <TextareaAutosize
            name='name'
            className='mainInfo'
            onClick={(e) => handleTips(e.target.className, section)}
            placeholder='Full Name'
            type='text'
            value={basicInfo.name}
            onChange={(e) => {
              handleBasicInfoInput(e.target.name, e.target.value);
            }}
          />
        </h2>

        <h5 className='title'>
          <TextareaAutosize
            name='title'
            placeholder='Professional title'
            type='text'
            value={basicInfo.title}
            onChange={(e) => {
              handleBasicInfoInput(e.target.name, e.target.value);
            }}
          />
        </h5>

        <p className='about'>
          <TextareaAutosize
            placeholder='About me'
            className='intro'
            onClick={(e) => handleTips(e.target.className, section)}
            name='goals'
            type='text'
            value={basicInfo.goals}
            onChange={(e) => {
              handleBasicInfoInput(e.target.name, e.target.value);
            }}
          />
        </p>
      </div>
    </div>
  );
}

export default BasicInfo;
