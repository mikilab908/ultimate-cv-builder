import React from 'react';
import ContactInfoInput from './ContactInfoInput';

function ContactInfo() {
  return (
    <>
      <h5 className='contact-heading'>CONTACT</h5>

      <ul className='contact-info'>
        <ContactInfoInput
          name='email'
          placeholder='Email'
          className='practicalPart'
        />

        <ContactInfoInput
          name='phone'
          placeholder='Phone Number'
          className='practicalPart'
        />

        <ContactInfoInput
          name='location'
          placeholder='City, Country'
          className='practicalPart'
        />

        <ContactInfoInput
          name='twitter'
          placeholder='Twitter'
          className='practicalPart'
        />
      </ul>
    </>
  );
}

export default ContactInfo;
