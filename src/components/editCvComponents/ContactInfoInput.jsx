import React from 'react';
import { useGlobalContext } from '../../context';

function ContactInfoInput(props) {
  const {
    section,
    handleTips,
    icons,
    contactInfo,
    handleContactInfo,
  } = useGlobalContext();

  return (
    <li>
      <div>
        <input
          type='text'
          placeholder={props.placeholder}
          onClick={(e) => handleTips(props.className, section)}
          value={contactInfo[props.name]}
          onChange={(e) => {
            handleContactInfo(props.name, e.target.value);
          }}
        />
        <span>{icons[props.name]}</span>
      </div>
    </li>
  );
}

export default ContactInfoInput;
