import React from 'react';
import { useGlobalContext } from '../../context';

function CvImage() {
  const {
    uploadedImg,
    fileUploadInputChange,
    section,
    handleTips,
  } = useGlobalContext();

  return (
    <div className='cv-image'>
      <label htmlFor='attach-image'>
        <img
          src={uploadedImg}
          className='add-image-logo'
          name='photo'
          onClick={(e) => handleTips(e.target.name, section)}
          alt='user-cv'
        />
      </label>
      <input id='attach-image' onChange={fileUploadInputChange} type='file' />
    </div>
  );
}

export default CvImage;
