import { React, useState } from 'react';
import RatingComponent from './RatingComponent';
import { useGlobalContext } from '../../context';
import AddButton from './AddButton';
import DeleteButton from './DeleteButton';
import { v4 as uuid } from 'uuid';

function DataScienceSkills() {
  const [skillFocused, setSkillFocused] = useState(false);
  const {
    dataScienceSkills,
    dataScienceSkillsChangeHandler,
    dataScienceNameSkillsChangeHandler,
    addDataScienceSkills,
    deleteDataScienceSkills,
    handleTips,
    section,
  } = useGlobalContext();

  return (
    <div className='skills-data-science'>
      <h5>SKILLS</h5>

      <AddButton
        name='addButton'
        function={() =>
          addDataScienceSkills({
            id: uuid(),
            skill: 'New Skill',
            level: 1,
          })
        }
        isIgnored={true}
      />

      {dataScienceSkills &&
        dataScienceSkills.map((element, i) => (
          <div
            className='data-science-skills'
            style={{ position: 'relative' }}
            key={i}
          >
            <input
              type='text'
              className='skills'
              name={element.skill}
              value={element.skill}
              onFocus={() => setSkillFocused(true)}
              onBlur={() => setSkillFocused(false)}
              onChange={(e) =>
                dataScienceNameSkillsChangeHandler(e, element.id)
              }
              onClick={(e) => handleTips(e.target.className, section)}
            />
            <RatingComponent
              name={element.skill}
              value={element.level}
              handler={(e) => dataScienceSkillsChangeHandler(e, element.id)}
            />
            {skillFocused && (
              <DeleteButton
                name='deleteButton'
                function={() => deleteDataScienceSkills(element.id)}
              />
            )}
          </div>
        ))}
    </div>
  );
}

export default DataScienceSkills;
