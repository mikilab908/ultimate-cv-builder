import React from 'react';
import { useGlobalContext } from '../../context';

function DeleteButton(props) {
  const { icons } = useGlobalContext();

  return (
    <div
      className='deleteButton'
      onMouseDown={props.function}
      data-html2canvas-ignore={props.isIgnored}
    >
      {icons[props.name]}
    </div>
  );
}

export default DeleteButton;
