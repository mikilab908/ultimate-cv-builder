import React, { useEffect } from 'react';
import '../../assets/css/EditDesign.css';
import BasicInfo from '../../components/editCvComponents/BasicInfo';
import WorkExperience from '../../components/editCvComponents/WorkExperience';
import ContactInfo from '../../components/editCvComponents/ContactInfo';
import Education from '../../components/editCvComponents/Education';
import Skills from '../../components/editCvComponents/Skills';
import { useGlobalContext } from '../../context';

const EditDesign = () => {
  const { handlePageToPdf } = useGlobalContext();
  let page = document.querySelector('#downloadPageToPdf');

  useEffect(() => {
    handlePageToPdf(page);
  });

  return (
    <div className='EditDesign' id='downloadPageToPdf'>
      <div className='row'>
        <div className='col-md-12 designer-content'>
          <BasicInfo />
          <div className='row'>
            <div className='col-md-6 designer-left'>
              <WorkExperience />
              <ContactInfo />
            </div>
            <div className='col-md-6 designer-right'>
              <Education />
              <Skills />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditDesign;
