import React, { useEffect } from 'react';
import BasicInfo from './BasicInfo';
import '../../assets/css/EditDevelopment.css';
import CvImage from './CvImage';
import ContactInfo from './ContactInfo';
import WorkExperience from './WorkExperience';
import Education from './Education';
import Skills from './Skills';
import Achievements from './Achievements';
import Languages from './Languages';
import { useGlobalContext } from '../../context';

const EditDevelopment = () => {
  const { handlePageToPdf } = useGlobalContext();
  let page = document.querySelector('#downloadPageToPdf');

  useEffect(() => {
    handlePageToPdf(page);
  });

  return (
    <div className='EditDevelopment' id='downloadPageToPdf'>
      <div className='row'>
        <div className='col-md-7 EditDevelopment-basicinfo'>
          <div className='row'>
            <div className='col-md-9'>
              <BasicInfo />
            </div>
            <div className='col-md-3'>
              <CvImage />
            </div>
          </div>
        </div>
        <div className='col-md-4 offset-md-1'>
          <ContactInfo />
        </div>
      </div>
      <hr />
      <div className='row development-form-wrap'>
        <div className='col-md-6'>
          <WorkExperience />
          <Education />
        </div>
        <div className='col-md-6'>
          <Skills />
          <Achievements />
          <Languages />
        </div>
      </div>
    </div>
  );
};

export default EditDevelopment;
