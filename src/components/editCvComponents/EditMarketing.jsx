import React, { useEffect } from 'react';
import '../../assets/css/EditMarketing.css';
import CVimage from '../../components/editCvComponents/CvImage';
import BasicInfo from '../../components/editCvComponents/BasicInfo';
import ContactInfo from '../../components/editCvComponents/ContactInfo';
import WorkExperience from '../../components/editCvComponents/WorkExperience';
import Education from '../../components/editCvComponents/Education';
import Skills from '../../components/editCvComponents/Skills';
import InformalEducation from '../../components/editCvComponents/InformalEducation';
import Achievements from '../../components/editCvComponents/Achievements';
import Languages from '../../components/editCvComponents/Languages';
import { useGlobalContext } from '../../context';

const DigitalMarketingCv = () => {
  const { handlePageToPdf } = useGlobalContext();
  let page = document.querySelector('#downloadPageToPdf');

  useEffect(() => {
    handlePageToPdf(page);
  });

  return (
    <div className='EditMarketing' id='downloadPageToPdf'>
      <div className='row'>
        <div className='col-md-12'>
          <div className='row'>
            <div className='col-md-3'>
              <CVimage />
            </div>
            <div className='col-md-9'>
              <BasicInfo />
            </div>
          </div>
          <div className='contact-bg'>
            <ContactInfo />
          </div>
          <div className='row'>
            <div className='col-md-6'>
              <WorkExperience />
              <Education />
            </div>
            <div className='col-md-6'>
              <Skills />
              <Achievements />
              <Languages />
              <InformalEducation />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DigitalMarketingCv;
