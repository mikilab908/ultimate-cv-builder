import React, { useEffect } from 'react';
import '../../assets/css/EditScience.css';
import Achievements from './Achievements';
import BasicInfo from './BasicInfo';
import ContactInfo from './ContactInfo';
import CvImage from './CvImage';
import Education from './Education';
import Languages from './Languages';
import WorkExperience from './WorkExperience';
import InformalEducation from './InformalEducation';
import DataScienceSkills from './DataScienceSkills';
import { useGlobalContext } from '../../context';

function EditScience() {
  const { handlePageToPdf } = useGlobalContext();
  let page = document.querySelector('#downloadPageToPdf');

  useEffect(() => {
    handlePageToPdf(page);
  });

  return (
    <div className='EditScience' id='downloadPageToPdf'>
      <div className='row'>
        <div className='col-md-12'>
          <div className='row'>
            <div className='col-md-3'>
              <CvImage />
            </div>
            <div className='col-md-9'>
              <BasicInfo />
            </div>
          </div>
          <hr />
          <ContactInfo />
          <hr />
        </div>
      </div>
      <div className='row science-form-wrap'>
        <div className='col-md-6'>
          <WorkExperience />
          <Education />
        </div>
        <div className='col-md-6'>
          <DataScienceSkills />
          <Achievements />
          <Languages />
          <InformalEducation />
        </div>
      </div>
    </div>
  );
}

export default EditScience;
