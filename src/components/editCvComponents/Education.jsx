import { React, useState } from 'react';
import TextareaAutosize from 'react-textarea-autosize';
import TimePeriod from './TimePeriod';
import { useGlobalContext } from '../../context';
import AddButton from './AddButton';
import DeleteButton from './DeleteButton';
import { v4 as uuid } from 'uuid';

const Education = () => {
  const {
    handleTips,
    section,
    education,
    handleEducation,
    editEducation,
    deleteEducation,
  } = useGlobalContext();
  const [focused, setFocused] = useState(true);

  return (
    <div className='education'>
      <h5>EDUCATION</h5>

      <AddButton
        name='addButton'
        function={() =>
          handleEducation({
            id: uuid(),
            study: '',
            institution: '',
            startMonth: '',
            startYear: '',
            endMonth: '',
            endYear: '',
            present: false,
            location: '',
            courses: '',
            projects: '',
          })
        }
        isIgnored={true}
      />

      {education &&
        education.map((el, idx) => (
          <div className='form-wrapper' id={el.id} key={idx}>
            <div className='side-box'>
              <div className='box'></div>
            </div>
            <div className='side-line'>
              <div className='line'></div>
            </div>

            <ul className='info-education'>
              {focused && (
                <DeleteButton
                  name='deleteButton'
                  function={() => deleteEducation(el.id)}
                  isIgnored={true}
                />
              )}

              <li>
                <h5 className='h5-study'>
                  <TextareaAutosize
                    name='study'
                    className='study education'
                    placeholder='Study Program'
                    type='text'
                    onFocus={() => setFocused(true)}
                    onBlur={() => setFocused(false)}
                    onClick={(e) =>
                      handleTips(e.target.className.split(' ')[1], section)
                    }
                    value={el.study}
                    onChange={(e) => editEducation(e, el.id, e.target.name)}
                  />
                </h5>
              </li>
              <li>
                <h5 className='h5-institution'>
                  <TextareaAutosize
                    name='institution'
                    className='institution education'
                    placeholder='Institution/University'
                    type='text'
                    onFocus={() => setFocused(true)}
                    onBlur={() => setFocused(false)}
                    value={el.institution}
                    onChange={(e) => editEducation(e, el.id, e.target.name)}
                    onClick={(e) =>
                      handleTips(e.target.className.split(' ')[1], section)
                    }
                  />
                </h5>
              </li>
              <li className='date-location'>
                <TimePeriod element={el} functionEdit={editEducation} />
              </li>
              <li className='studied'>
                <TextareaAutosize
                  name='courses'
                  className='courses'
                  placeholder='Studied/Courses'
                  type='text'
                  onFocus={() => setFocused(true)}
                  onBlur={() => setFocused(false)}
                  value={el.courses}
                  onChange={(e) => editEducation(e, el.id, e.target.name)}
                />
              </li>
              <li>
                <TextareaAutosize
                  name='projects'
                  className='projects'
                  placeholder='Course/Thesis/Project'
                  type='text'
                  onFocus={() => setFocused(true)}
                  onBlur={() => setFocused(false)}
                  value={el.projects}
                  onChange={(e) => editEducation(e, el.id, e.target.name)}
                />
              </li>
            </ul>
          </div>
        ))}
    </div>
  );
};

export default Education;
