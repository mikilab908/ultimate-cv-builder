import React from 'react';
import { useGlobalContext } from '../../context';
import AddButton from './AddButton';
import InformalEducationInput from './InformalEducationInput';

const InformalEducation = () => {
  const {
    informalEducation,
    handleTips,
    informalEducationChangeHandler,
    addInformalEducation,
    deleteInformalEducation,
    section,
  } = useGlobalContext();

  return (
    <div className='informal-education'>
      <h5>INFORMAL EDUCATION</h5>

      <div className='form-wrapper'>
        <AddButton
          name='addButton'
          function={() => addInformalEducation()}
          isIgnored={true}
        />
        {informalEducation &&
          informalEducation.map((skill, i) => (
            <InformalEducationInput
              name='informalEducation'
              key={i}
              function={(e) => informalEducationChangeHandler(e, i)}
              value={skill}
              delete={(e) => deleteInformalEducation(e, i)}
              tips={(e) => handleTips(e.target.name, section)}
            />
          ))}
      </div>
    </div>
  );
};

export default InformalEducation;
