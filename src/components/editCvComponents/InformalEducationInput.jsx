import React, { useState } from 'react';
import AutosizeInput from 'react-input-autosize';
import DeleteButton from './DeleteButton';

function InformalEducationInput(props) {
  const [skillFocused, setSkillFocused] = useState(false);

  return (
    <span style={{ position: 'relative' }}>
      <AutosizeInput
        name='informalEducation'
        onChange={props.function}
        value={props.value}
        type='text'
        maxLength='30'
        onClick={props.tips}
        onFocus={() => setSkillFocused(true)}
        onBlur={() => setSkillFocused(false)}
        disabled={props.disabled}
      />
      {skillFocused && (
        <DeleteButton name='deleteButton' function={props.delete} />
      )}
    </span>
  );
}
export default InformalEducationInput;
