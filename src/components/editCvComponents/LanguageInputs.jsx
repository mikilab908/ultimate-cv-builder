import React, { useState } from 'react';
import { useGlobalContext } from '../../context';
import DeleteButton from './DeleteButton';

function LanguageInputs({ idx, value, id }) {
  const {
    languageChangeHandler,
    deleteLanguage,
    section,
    handleTips,
  } = useGlobalContext();
  const [focused, setFocused] = useState(true);

  return (
    <div>
      <div style={{ position: 'relative' }}>
        <input
          className='languages'
          type='text'
          value={value.language}
          onChange={(e) => languageChangeHandler(e, idx)}
          onFocus={() => setFocused(true)}
          onBlur={() => setFocused(false)}
          name='language'
          onClick={(e) => handleTips(e.target.className, section)}
          placeholder='Language'
        />
        {focused && (
          <DeleteButton
            name='deleteButton'
            function={() => deleteLanguage(id)}
            isIgnored={true}
          />
        )}
      </div>
      {!focused ? (
        <input
          type='text'
          readOnly
          onFocus={() => setFocused(true)}
          className='language-level'
          value={
            value.level === 1
              ? 'Elementary Proficiency'
              : value.level === 2
              ? 'Limited Working Proficiency'
              : value.level === 3
              ? 'Professional Working Proficiency'
              : value.level === 4
              ? 'Full Professional Proficiency'
              : 'Native or Bilingual Proficiency'
          }
        />
      ) : (
        <div>
          <span
            onMouseDown={(e) => languageChangeHandler(e, idx)}
            className='1'
            onClick={() => setFocused(false)}
          >
            1/5
          </span>
          <span
            onMouseDown={(e) => languageChangeHandler(e, idx)}
            className='2'
            onClick={() => setFocused(false)}
          >
            2/5
          </span>
          <span
            onMouseDown={(e) => languageChangeHandler(e, idx)}
            className='3'
            onClick={() => setFocused(false)}
          >
            3/5
          </span>
          <span
            onMouseDown={(e) => languageChangeHandler(e, idx)}
            className='4'
            onClick={() => setFocused(false)}
          >
            4/5
          </span>
          <span
            onMouseDown={(e) => languageChangeHandler(e, idx)}
            className='5'
            onClick={() => setFocused(false)}
          >
            5/5
          </span>
        </div>
      )}
    </div>
  );
}

export default LanguageInputs;
