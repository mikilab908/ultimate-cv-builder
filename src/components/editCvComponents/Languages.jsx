import React from 'react';
import { useGlobalContext } from '../../context';
import LanguageInputs from './LanguageInputs';
import AddButton from './AddButton';
import { v4 as uuid } from 'uuid';

const Languages = () => {
  const { languages, addLanguage } = useGlobalContext();

  return (
    <div className='languages language-form-wrap'>
      <h5>LANGUAGES</h5>

      <AddButton
        name='addButton'
        function={() =>
          addLanguage({
            id: uuid(),
            language: '',
            level: '',
          })
        }
        isIgnored={true}
      />

      {languages &&
        languages.map((element, i) => (
          <div className='form-wrapper' key={i}>
            <LanguageInputs idx={i} value={element} id={element.id} />
          </div>
        ))}
    </div>
  );
};

export default Languages;
