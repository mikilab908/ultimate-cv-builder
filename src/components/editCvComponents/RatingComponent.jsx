import React from 'react';
import StarRatingComponent from 'react-star-rating-controlled-component';
import RatingIcon from './RatingIcon';

const RatingComponent = (props) => {
  return (
    <StarRatingComponent
      name={props.name}
      value={props.value}
      onStarClick={(e) => props.handler(e, props.name)}
      renderStarIcon={() => <span>{<RatingIcon name='fullCircle' />}</span>}
      starColor='#364458'
      emptyStarColor='white'
    />
  );
};

export default RatingComponent;
