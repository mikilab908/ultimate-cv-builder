import React from 'react';
import { useGlobalContext } from '../../context';

function RatingIcon(props) {
  const { icons } = useGlobalContext();

  return (
    <div className='ratingIcon' onClick={props.function}>
      {icons[props.name]}
    </div>
  );
}

export default RatingIcon;
