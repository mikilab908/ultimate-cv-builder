import React from 'react';
import { useGlobalContext } from '../../context';
import SkillInput from './SkillInput';
import AddButton from './AddButton';

const Skills = () => {
  const {
    skills,
    handleTips,
    skillsChangehandler,
    addSkill,
    deleteSkill,
    section,
  } = useGlobalContext();

  return (
    <div className='skills'>
      <h5>SKILLS & COMPETENCIES</h5>

      <div className='form-wrapper'>
        <AddButton
          name='addButton'
          function={() => addSkill('skills')}
          isIgnored={true}
        />
        {skills &&
          skills.map((skill, i) => (
            <SkillInput
              name='skills'
              key={i}
              function={(e) => skillsChangehandler(e, i)}
              value={skill}
              delete={(e) => deleteSkill(e, i)}
              tips={(e) => handleTips(e.target.name, section)}
            />
          ))}
      </div>
    </div>
  );
};

export default Skills;
