import React, { useState } from 'react';
import '../../assets/css/TimePeriod.css';

function TimePeriod({ element, functionEdit }) {
  const [focused, setFocused] = useState(false);

  return (
    <div className='time-period'>
      <span>
        <input
          name='startMonth'
          className='month startMonth'
          type='text'
          placeholder='mm'
          maxLength='2'
          value={element.startMonth}
          onChange={(e) => functionEdit(e, element.id, e.target.name)}
        />{' '}
        <span className='period-separator'>/</span>{' '}
        <input
          name='startYear'
          className='year'
          type='text'
          placeholder='yyyy'
          maxLength='4'
          value={element.startYear}
          onChange={(e) => functionEdit(e, element.id, e.target.name)}
        />{' '}
        <span className='dash-separator'>-</span>{' '}
        {element.present && (
          <input
            className='present'
            value='Present'
            readOnly
            onFocus={() => setFocused(true)}
            onBlur={() => setFocused(false)}
          />
        )}
        {!element.present && (
          <React.Fragment>
            <input
              name='endMonth'
              className='month endMonth'
              type='text'
              placeholder='mm'
              maxLength='2'
              onFocus={() => setFocused(true)}
              onBlur={() => setFocused(false)}
              value={element.endMonth}
              onChange={(e) => functionEdit(e, element.id, e.target.name)}
            />{' '}
            <span className='period-separator'>/</span>{' '}
            <input
              name='endYear'
              className='year'
              type='text'
              placeholder='yyyy'
              maxLength='4'
              onFocus={() => setFocused(true)}
              onBlur={() => setFocused(false)}
              value={element.endYear}
              onChange={(e) => functionEdit(e, element.id, e.target.name)}
            />
          </React.Fragment>
        )}
        {focused && (
          <React.Fragment>
            {' '}
            <input
              id='present-checkbox'
              name='present'
              type='checkbox'
              readOnly
              className='present-input'
              checked={element.present}
              onMouseDown={(e) =>
                functionEdit(e, element.id, e.target.name, element)
              }
              onBlur={() => setFocused(false)}
            />
            <label htmlFor='present-checkbox' className='present-label'>
              Present
            </label>
          </React.Fragment>
        )}
        <span>
          <input
            name='location'
            className='location'
            type='text'
            placeholder='City, Country'
            value={element.location}
            onChange={(e) => functionEdit(e, element.id, e.target.name)}
          />
        </span>
      </span>
    </div>
  );
}

export default TimePeriod;
