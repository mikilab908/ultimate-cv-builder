import { React, useState } from 'react';
import TextareaAutosize from 'react-textarea-autosize';
import TimePeriod from './TimePeriod';
import { useGlobalContext } from '../../context';
import AddButton from './AddButton';
import DeleteButton from './DeleteButton';
import { v4 as uuid } from 'uuid';

const WorkExperience = () => {
  const {
    workExperience,
    handleWorkExperience,
    editWorkExperience,
    deleteWorkExperience,
    section,
    handleTips,
  } = useGlobalContext();
  const [focused, setFocused] = useState(true);

  return (
    <div className='work-experience'>
      <h5>WORK EXPERIENCE</h5>

      <AddButton
        name='addButton'
        function={() =>
          handleWorkExperience({
            id: uuid(),
            title: '',
            workplace: '',
            startMonth: '',
            startYear: '',
            endMonth: '',
            endYear: '',
            present: false,
            location: '',
            description: '',
            achievements: '',
            responsibility: '',
          })
        }
        isIgnored={true}
      />

      {workExperience &&
        workExperience.map((el, idx) => (
          <div className='form-wrapper' id={el.id} key={idx}>
            <div className='side-box'>
              <div className='box'></div>
            </div>
            <div className='side-line'>
              <div className='line'></div>
            </div>

            <ul className='info-worklist'>
              {focused && (
                <DeleteButton
                  name='deleteButton'
                  function={() => deleteWorkExperience(el.id)}
                  isIgnored={true}
                />
              )}

              <li>
                <h5 className='h5-title'>
                  <TextareaAutosize
                    name='title'
                    className='title previousJob'
                    placeholder='Title/Position'
                    value={el.title}
                    onFocus={() => setFocused(true)}
                    onBlur={() => setFocused(false)}
                    onChange={(e) =>
                      editWorkExperience(e, el.id, e.target.name)
                    }
                    type='text'
                    onClick={(e) =>
                      handleTips(e.target.className.split(' ')[1], section)
                    }
                  />
                </h5>
              </li>
              <li>
                <h5 className='h5-workplace'>
                  <TextareaAutosize
                    name='workplace'
                    className='workplace previousJob'
                    placeholder='Workplace/Company'
                    value={el.workplace}
                    onFocus={() => setFocused(true)}
                    onBlur={() => setFocused(false)}
                    onChange={(e) =>
                      editWorkExperience(e, el.id, e.target.name)
                    }
                    type='text'
                    onClick={(e) =>
                      handleTips(e.target.className.split(' ')[1], section)
                    }
                  />
                </h5>
              </li>
              <li className='date-location'>
                <TimePeriod element={el} functionEdit={editWorkExperience} />
              </li>
              <li>
                <TextareaAutosize
                  name='description'
                  className='description'
                  placeholder='Company Description (Optional)'
                  type='text'
                  onFocus={() => setFocused(true)}
                  onBlur={() => setFocused(false)}
                  value={el.description}
                  onChange={(e) => editWorkExperience(e, el.id, e.target.name)}
                />
              </li>
              <li className='accomplishments'>
                <TextareaAutosize
                  name='achievements'
                  className='achievements'
                  placeholder='Achievements/Tasks'
                  type='text'
                  onFocus={() => setFocused(true)}
                  onBlur={() => setFocused(false)}
                  value={el.achievements}
                  onChange={(e) => editWorkExperience(e, el.id, e.target.name)}
                />
              </li>
              <li>
                <TextareaAutosize
                  name='responsibility'
                  className='responsibility'
                  placeholder='Responsibility/Task'
                  type='text'
                  onFocus={() => setFocused(true)}
                  onBlur={() => setFocused(false)}
                  value={el.responsibility}
                  onChange={(e) => editWorkExperience(e, el.id, e.target.name)}
                />
              </li>
            </ul>
          </div>
        ))}
    </div>
  );
};
export default WorkExperience;
