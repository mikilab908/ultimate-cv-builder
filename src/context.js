import React, { useState, useContext, useEffect } from 'react';
import { tips, iconsObj, initLaikaTip, initCvTip, cvBuildForm } from './data';
import html2pdf from 'html2pdf.js';

const AppContext = React.createContext();

const AppProvider = ({ children }) => {
  const [isCvChoosen, setIsCvChoosen] = useState('');
  const [isEditMode, setIsEditMode] = useState(false);
  const [isActiveClass, setIsActiveClass] = useState(true);
  const [targetName, setTargetName] = useState('');
  const [location, setLocation] = useState('');
  const [section, setSection] = useState();
  const [endLocation, setEndLocation] = useState('');
  const [displayTips, setDisplayTips] = useState();
  const [icons] = useState(iconsObj);
  const [basicInfo, setBasicInfo] = useState(
    cvBuildForm[0].cvElements.basicInfo
  );
  const [uploadedImg, setuploadedImg] = useState(
    cvBuildForm[0].cvElements.image
  );
  const [contactInfo, setContactInfo] = useState(
    cvBuildForm[0].cvElements.contactInfo
  );
  const [workExperience, setWorkExperience] = useState(
    cvBuildForm[0].cvElements.workExperienceArr
  );
  const [education, setEducation] = useState(
    cvBuildForm[0].cvElements.educationArr
  );
  const [skills, setSkills] = useState(cvBuildForm[0].cvElements.skillsArr);
  const [achievements, setAchievements] = useState(
    cvBuildForm[0].cvElements.achievementsArr
  );
  const [languages, setLanguages] = useState(
    cvBuildForm[0].cvElements.languagesArr
  );
  const [dataScienceSkills, setDataScienceSkills] = useState(
    cvBuildForm[0].cvElements.dataScienceSkillsArr
  );
  const [informalEducation, setInformalEducation] = useState(
    cvBuildForm[0].cvElements.informalEducationArr
  );

  const [cvFilled, setCvFilled] = useState([
    { basicInfoFilled: false },
    { contactInfoFilled: false },
    { workExperienceFilled: false },
    { educationFilled: false },
    { skillsFilled: false },
    { languagesFilled: false },
  ]);

  const [isLinkedChecked, setLinkedChecked] = useState(false);
  const [isLaikaChecked, setLaikaChecked] = useState(false);
  const [fieldsFilled, setFieldFilled] = useState();
  const [modal, setModal] = useState(false);
  const [pageToPdf, setPageToPdf] = useState();

  const PdfDownloadCheck = () => {
    function basicinfocheck() {
      if (basicInfo.name.length > 0 && basicInfo.title.length > 0) {
        setCvFilled([...cvFilled, (cvFilled[0].basicInfoFilled = true)]);
      }
    }
    function contactInfocheck() {
      if (contactInfo.email !== '') {
        setCvFilled([...cvFilled, (cvFilled[1].contactInfoFilled = true)]);
      }
    }
    function workExperiencecheck() {
      if (
        workExperience[0].title.length > 0 &&
        workExperience[0].workplace.length > 0
      ) {
        setCvFilled([...cvFilled, (cvFilled[2].workExperienceFilled = true)]);
      }
    }
    function educationcheck() {
      if (
        education[0].study.length > 0 &&
        education[0].institution.length > 0
      ) {
        setCvFilled([...cvFilled, (cvFilled[3].educationFilled = true)]);
      }
    }
    function skillscheck() {
      if (skills.length > 1) {
        setCvFilled([...cvFilled, (cvFilled[4].skillsFilled = true)]);
      }
    }
    function languagescheck() {
      if (languages.length > -1) {
        setCvFilled([...cvFilled, (cvFilled[5].languagesFilled = true)]);
      }
    }
    basicinfocheck();
    contactInfocheck();
    workExperiencecheck();
    educationcheck();
    skillscheck();
    languagescheck();

    let fields = 0;
    cvFilled.forEach((el) => {
      Object.entries(el).forEach(([key, value]) => {
        if (value === true) {
          fields++;
        }
      });
    });
    setFieldFilled(fields);
  };

  useEffect(() => {
    if (section === 'linked' || section === 'laika') {
      setDisplayTips(initLaikaTip);
    } else {
      setDisplayTips(initCvTip);
    }
  }, [section]);

  const chooseCvRoute = (route) => {
    setIsCvChoosen(route);
  };

  const handleEditMode = () => {
    setIsEditMode(!isEditMode);
  };

  const handleActiveClass = (e) => {
    setIsActiveClass(false);
    setTargetName(e.target.text);
  };

  const handleTips = (name, section) => {
    const key = tips[section];
    setDisplayTips(key[name]);
  };

  const handleLocation = (pathname) => {
    setLocation(pathname.pathname);
    let section = location.split('/')[2];
    setSection(section);
  };

  const handleEndLocation = (location) => {
    setEndLocation(location);
  };

  const handleBasicInfoInput = (name, value) => {
    setBasicInfo({ ...basicInfo, [name]: value });
  };

  const fileUploadInputChange = (e) => {
    e.target.value && setuploadedImg(URL.createObjectURL(e.target.files[0]));
  };

  const handleContactInfo = (name, value) => {
    setContactInfo({ ...contactInfo, [name]: value });
  };

  const handleWorkExperience = (exp) => {
    setWorkExperience([...workExperience, exp]);
  };

  const editWorkExperience = (e, id, name) => {
    setWorkExperience([
      ...workExperience.map((el) => {
        if (el.id === id) {
          if (name === 'present') {
            el[name] = !e.target.checked;
            return el;
          }
          el[name] = e.target.value;
        }
        return el;
      }),
    ]);
  };

  const deleteWorkExperience = (id) => {
    setWorkExperience([...workExperience.filter((exp) => exp.id !== id)]);
  };

  const editEducation = (e, id, name) => {
    setEducation([
      ...education.map((el) => {
        if (el.id === id) {
          if (name === 'present') {
            el[name] = !e.target.checked;
            return el;
          }
          el[name] = e.target.value;
        }
        return el;
      }),
    ]);
  };

  const handleEducation = (newEdu) => {
    setEducation([...education, newEdu]);
  };

  const deleteEducation = (id) => {
    setEducation([...education.filter((edu) => edu.id !== id)]);
  };

  const skillsChangehandler = (e, index) => {
    setSkills([
      ...skills.map((skill, idx) => {
        if (idx === index) {
          skill = e.target.value;
        }
        return skill;
      }),
    ]);
  };

  const addSkill = () => {
    setSkills([...skills, 'New Skill']);
  };

  const deleteSkill = (e, i) => {
    setSkills([...skills.filter((skill, idx) => idx !== i)]);
  };

  const informalEducationChangeHandler = (e, index) => {
    setInformalEducation([
      ...informalEducation.map((skill, idx) => {
        if (idx === index) {
          skill = e.target.value;
        }
        return skill;
      }),
    ]);
  };

  const addInformalEducation = () => {
    setInformalEducation([...informalEducation, 'Informal Education']);
  };

  const deleteInformalEducation = (e, i) => {
    setInformalEducation([
      ...informalEducation.filter((skill, idx) => idx !== i),
    ]);
  };

  const handleAchievements = (newAch) => {
    setAchievements([...achievements, newAch]);
  };

  const deleteAchievement = (id) => {
    setAchievements([...achievements.filter((el) => el.id !== id)]);
  };

  const editAchievements = (e, id, name) => {
    setAchievements([
      ...achievements.map((el) => {
        if (el.id === id) {
          el[name] = e.target.value;
        }
        return el;
      }),
    ]);
  };

  const languageChangeHandler = (e, idx) => {
    const { name, value, className } = e.target;

    if (name === 'language') {
      setLanguages([
        ...languages.map((el, index) => {
          if (index === idx) {
            el.language = value;
          }
          return el;
        }),
      ]);
    } else {
      setLanguages([
        ...languages.map((el, index) => {
          if (index === idx) {
            el.level = parseInt(className);
          }
          return el;
        }),
      ]);
    }
  };

  const addLanguage = (language) => {
    setLanguages([...languages, language]);
  };

  const deleteLanguage = (id) => {
    setLanguages([...languages.filter((el) => el.id !== id)]);
  };

  const dataScienceSkillsChangeHandler = (e, id) => {
    setDataScienceSkills([
      ...dataScienceSkills.map((el) => {
        if (el.id === id) {
          el.level = e;
        }
        return el;
      }),
    ]);
  };

  const dataScienceNameSkillsChangeHandler = (e, id) => {
    setDataScienceSkills([
      ...dataScienceSkills.map((el) => {
        if (el.id === id) {
          el.skill = e.target.value;
        }
        return el;
      }),
    ]);
  };

  const addDataScienceSkills = (newSkill) => {
    setDataScienceSkills([...dataScienceSkills, newSkill]);
  };

  const deleteDataScienceSkills = (id) => {
    setDataScienceSkills([...dataScienceSkills.filter((el) => el.id !== id)]);
  };

  const handleDownloadPdf = () => {
    if (fieldsFilled >= 6 && isLinkedChecked && isLaikaChecked) {
      window.scrollTo(0, 0);
      let opt = {
        margin: [5, 5, 5, 5],
        filename: '',
        image: { type: 'png', quality: 1 },
        html2canvas: { scale: 2, useCORS: true },
        jsPDF: { unit: 'mm', format: 'letter' },
      };
      html2pdf().from(pageToPdf).set(opt).save();
    } else {
      setModal(true);
    }
  };

  const handleLinkedIn = (value) => {
    setLinkedChecked(value);
  };

  const handleLaika = (value) => {
    setLaikaChecked(value);
  };

  const handleClose = () => {
    setModal(false);
  };

  const handlePageToPdf = (page) => {
    setPageToPdf(page);
  };

  return (
    <AppContext.Provider
      value={{
        isCvChoosen,
        chooseCvRoute,
        isEditMode,
        handleEditMode,
        isActiveClass,
        handleActiveClass,
        targetName,
        displayTips,
        handleTips,
        location,
        section,
        handleLocation,
        endLocation,
        handleEndLocation,
        icons,
        uploadedImg,
        fileUploadInputChange,
        workExperience,
        handleWorkExperience,
        editWorkExperience,
        deleteWorkExperience,
        skillsChangehandler,
        skills,
        addSkill,
        deleteSkill,
        initLaikaTip,
        initCvTip,
        basicInfo,
        handleBasicInfoInput,
        contactInfo,
        handleContactInfo,
        education,
        editEducation,
        handleEducation,
        deleteEducation,
        achievements,
        handleAchievements,
        deleteAchievement,
        editAchievements,
        languages,
        languageChangeHandler,
        addLanguage,
        deleteLanguage,
        dataScienceSkills,
        dataScienceSkillsChangeHandler,
        dataScienceNameSkillsChangeHandler,
        addDataScienceSkills,
        informalEducation,
        informalEducationChangeHandler,
        addInformalEducation,
        deleteInformalEducation,
        deleteDataScienceSkills,
        handleDownloadPdf,
        handleLinkedIn,
        handleLaika,
        isLinkedChecked,
        isLaikaChecked,
        PdfDownloadCheck,
        fieldsFilled,
        modal,
        handleClose,
        handlePageToPdf,
      }}
    >
      {children}{' '}
    </AppContext.Provider>
  );
};

//custom hook
export const useGlobalContext = () => {
  return useContext(AppContext);
};

export { AppContext, AppProvider };
