import { v4 as uuid } from 'uuid';
import imgLogo from '../src/assets/images/addimg.png';

import {
  FaMobileAlt,
  FaRegEnvelope,
  FaTwitter,
  FaMapMarkerAlt,
  FaPlus,
  FaTrash,
  FaCircle,
} from 'react-icons/fa';

export const iconsObj = {
  phone: <FaMobileAlt />,
  email: <FaRegEnvelope />,
  twitter: <FaTwitter />,
  location: <FaMapMarkerAlt />,
  addButton: <FaPlus />,
  deleteButton: <FaTrash />,
  fullCircle: <FaCircle />,
};

export const categories = [
  {
    id: 1,
    title: 'Web Development',
    route: 'development',
  },
  {
    id: 2,
    title: 'Data Science',
    route: 'science',
  },
  {
    id: 3,
    title: 'Digital Marketing',
    route: 'marketing',
  },
  {
    id: 4,
    title: 'Design',
    route: 'design',
  },
];

export const initLaikaTip = 'Detailed tips and suggestions on every field.';
export const initCvTip =
  "Here's where you get comments on each section of the CV, e.g. you click on the photo and it gives you tips on how it should look like.";

export const tips = {
  development: {
    photo:
      'Your photo should be professional. It’s better to send a CV without a photo, that with one that makes you seem unserious.',

    mainInfo:
      'Write a short intro that truly represents you – not cheesy quotes, but something you truly believe in. \nTailor the CV according to job you’re applying for. \nThere is no ‘one size fits all’ CV – so always adapt it before applying to a job.',

    intro:
      'Write your resume in the language that you would use at your workplace. This is a great chance to show the company’s representatives that you can express yourself. \nDon’t put overused buzzwords, describe yourself and your capabilities with your own vocabulary.',

    previousJob:
      'Write your previous job experience and if you’re currently working, your current job titles. Explain your responsibilities and projects and list the tech stack and products/projects you worked on.  \nIf you were working in a team, let the recruiter know what your role was. Dates – a time frame in which you have been on a certain job title. If you are still working there, leave a hyphen ( - ). Leave your most recent experience first, and the oldest ones last. Tip: Use Action verbs to demonstrate your experience. Examples include: “applied”, “improved”, “implemented” etc.',

    practicalPart:
      'Email, phone number and date of birth are the practical part of the assembly of a CV. Enter your professional email address, preferably on Gmail. Your city of living is an information that interests your potential employers. But don’t leave your home address. Leave a link to your LinkedIn profile. \nIf you have proof of your previous job experiences online, link that too.',

    skills:
      'Showcase your tech stack. List the stack under every previous project you have worked on, or showcase your projects on GitHub. The easiest way to do this is to list the stack under a previous project that you have worked on. Another way is to showcase your projects on GitHub. Use numbers. If you helped a website scale, put a metric in there.',

    education:
      'Education is an important section – but be sure to select only what is relevant to the job you’re applying for. Write about your university degrees, relevant courses and if you have any publications, but skip the high and elementary school. Don’t be afraid to include a “Informal Education” section, where you can list all the courses, podcasts and webinars that you used to teach yourself about software development. Don’t include everything – just the education that is relevant and necessary for the position you are applying for.',

    achievements:
      'Tip: Highlight the key achievements you have presented on your CV so the hiring manager can catch them very quickly.',

    languages:
      'All languages, even those you don’t speak fluently, can help you get a certain position. It’s good to use expressions such as “fluent in”, “speaking level of”, “native language” and similar.',
  },

  science: {
    intro:
      'This is a great chance to show the company’s representatives that you can express yourself. \nDon’t put overused buzzwords, describe yourself and your capabilities with your own vocabulary.',

    photo:
      'Put a photo made with a good camera. It’s better to send a CV without a photo, if you were planning on cropping yourself from a group photo from a dinner with friends, or a selfie in you room.',

    previousJob:
      'Write your previous job experience, and if you’re currently working, your current job title. Explain your responsibilities and projects and list the tech stack and products/projects you worked on. If you were working in a team, let the recruiter know what your role was. Dates – a time frame in which you have been on a certain job title. If you are still working there, leave a hyphen ( - ). Leave your most recent experience first, and the oldest ones last. We want to enable the reader to see what he needs to about your experience right away. Describe every job title with a few words about your most relevant experiences connected to the title you’re applying for. Tip: Use Action verbs to demonstrate your experience. Examples include: “applied”, “improved”, “implemented” etc.',

    education:
      'Let the recruiter know about your education, but only write the important parts – skip the high school and elementary, stick to the university degrees and courses relevant to the job you’re applying for.',

    achievements:
      'Immediately should be your Projects/Publications section. In the tech industry, the focus should be on what you have created. Include data analysis projects, machine learning projects, and if possible, published scientific articles or tutorials. Pick projects with relevance and connection to the job you’re applying for. They should demonstrate your technical skills and how they are applicable to solving real problems.',

    skills:
      'Be specific about the skills, tech and tools you used, and what your role was if you’re listing group projects. Specify coding languages, libraries etc. Make sure you use keywords that will put the focus on your best skills. If an HR/recruiter just scans your CV, they will catch their attention. Highlight terms like “Python” or “Machine learning”. Space is limited, so don’t waste t on soft skills like leadership or communication.',

    mainInfo:
      'Write your resume in the language that you would use at your workplace. Keep it brief. \n It might require more work and research, but customize your CV according to the position you are applying for. Add small details in some places in accordance to the job description. Tip: Speak in the third person, in bullet form. Don’t write long sentences, try to go straight to the point. Try to incorporate the answers to these questions: What did they commend me for? Which technologies and tools did I use? Did I implement a new idea? Order: Start with the skills and recent job positions. Languages and other qualifications should come after that. If you need space, leave out your personal interests, or at least use a smaller font for them.',

    languages:
      'All languages, even those you don’t speak fluently, can help you get a certain position. It’s good to use expressions such as “fluent in”, “speaking level of”, “native language” and similar.',

    informalEducation:
      'Don’t be afraid to include a “Informal education” where you can list all the courses, conferences, workshops and webinars that you used to teach yourself about digital marketing. Don’t include everything – just the education that is relevant and necessary for the position you are applying for.',

    practicalPart:
      'Email, phone number and date of birth are the practical part of the assembly of a CV. Enter your professional email address, preferably on Gmail. Your city of living is an information that interests your potential employers. Don’t leave your home address. Include your LinkedIn profile link, but don’t just copy and paste the whole profile URL, shorten it. Add a GitHub link or personal profile link to your contact information, and make it clickable. You’re applying for data science jobs, so most employers are going to look at your portfolio to see what kinds of projects you’re working on.',
  },

  marketing: {
    intro: 'Tailor your CV to the company and position you’re applying for.',

    photo:
      'Put a photo that looks professional – made with a good camera and showing you in a warm light. It’s better to send a CV without a photo, than a photo that makes you seem unprofessional.',

    mainInfo:
      'Write your resume in the language that you would use at your workplace. This is a great chance to show the company’s representatives that you can express yourself. Know your unique value proposition and communicate it effectively. You need a personal tagline that will help you stand out from everyone else. This line should be the first impression the hiring manager will get from you. E.g. if you’re a Content Writer, don’t just say ‘’I’m a great content writer’’. Be creative and say why they should hire you to handle their content. Do your research in advance to create a value proposition relevant to the company you’re applying to.',

    previousJob:
      'List the job experiences that are relevant to the position you’re applying for. If you’re currently working, your current job titles. Explain your responsibilities and projects and list the strategies and projects you created or worked on.  Highlight the keywords from the job specification on your CV. For example, if you’re applying for an e-commerce role, include keywords such as ‘’Conversion, bounce rate and Google Analytics’’. Talk about campaigns you worked on, the budget you had, how it benefited the company etc. This will help recruiters learn how you could benefit them based on your past experience. Dates – a time frame in which you have been on a certain job title. If you are still working there, leave a hyphen ( - ). Leave your most recent experience first, and the oldest ones last. Enable the reader to see what he needs to about your experience right away. Describe every job title with a few words about your most relevant experiences connected to the title you’re applying for. Tip: Use Action verbs to demonstrate your experience. Examples include: “applied”, “improved”, “implemented” etc.',

    education:
      'Under education, list only the relevant studies and courses, like university degree or Digital Marketing Academy. Don’t write where you went to high school. Don’t be afraid to include a “Self-Study” where you can list all the courses, conferences, workshops and webinars that you used to teach yourself about digital marketing. Don’t include everything – just the education that is relevant and necessary for the position you are applying for.',

    practicalPart:
      'Email, phone number and date of birth are the practical part of the assembly of a CV. Enter your professional email address, preferably on Gmail. Your city of living is an information that interests your potential employers. Don’t leave your home address.',

    skills:
      'Analyze what your key skills are. Make sure to list the skills you can shine in from day one. Showcase all the tools and technologies you know how to use. Everything that’s useful to a marketer: from Typeform and Canva, to Photoshop and Data Studio. Link campaigns you worked on If possible. Use numbers. If you helped a social media page gain a lot of followers, put the number in. If your campaign influenced a spike in sales, write in the percentage. Also, explain how you got there too.',

    achievements:
      'Tip: Highlight the key achievements you have presented on your CV so the hiring manager can catch them very quickly.',

    languages:
      'All languages, even those you don’t speak fluently, can help you get a certain position. It’s good to use expressions such as “fluent in”, “speaking level of”, “native language” and similar.',

    informalEducation:
      'Don’t be afraid to include a “Informal education” where you can list all the courses, conferences, workshops and webinars that you used to teach yourself about digital marketing. Don’t include everything – just the education that is relevant and necessary for the position you are applying for.',
  },

  design: {
    mainInfo:
      'Write your resume in the language that you would use at your workplace. This is a great chance to show the company’s representatives that you can express yourself. One CV isn’t suitable for every job position. Adapt your CV for the job position you’re applying for. Templates aren’t suitable for a designer, except if you are applying to a job call that explicitly wants that format. ',

    intro:
      'Write a short intro that truly represents you – not a cheesy quote, but something you truly believe in. Tip: Play with the font size. The visual part of the CV is also important, so use different colors and dimensions. Advice: This builder is a great start to help you with the content of the CV, but try making it in Photoshop or Illustrator.',

    previousJob:
      'Describe every job experience with a few words about your most relevant responsibilities and projects connected to the title you’re applying for. Dates are a time frame in which you have been on a certain job title. If you are still working there, leave a hyphen ( - ). First write your most recent experience, and the oldest ones last. We want to enable the reader to see what he needs to about your experience right away. Make it just one page, and don’t write all the projects you’ve worked on – only the ones relevant to the position. ',

    education:
      'List relevant education, including workshops or lectures you have visited. If you have a bachelor in Sports, and now you are applying as a designer because you finished a course, the course should be the highest in your “education” section. Don’t put in high and elementary school.',

    skills: 'List all the skills, tools and technologies that you know well.',

    practicalPart:
      'Email, phone number and date of birth are the practical part of the assembly of a CV. Enter your professional email address, preferably on Gmail. Your city of living is an information that interests your potential employers. Don’t leave your home address. Leave a link to your LinkedIn profile, or Behance/Dribbble/WiX. Make sure it’s clickable, and shorten it.',
  },

  linked: {
    title:
      'Make sure that your title matches your skills. E.g., if you only finished an academy or course, don’t say you are a Graphic Design Expert. Skip the abbreviations and titles – keep it short and modest.',
    about:
      'Be concise and easy to understand. Don’t use overdone motivational quotes about work ethics and inspiration, try to make the ‘about’ section as personal and as reflective of you as possible. \nUse the new “Open for business” feature. State your area of expertise and industry, to let recruiters know they can reach you.\nDon’t write in things that you wouldn’t like to happen. For example, if you say you work good under pressure, employers might have an ace up their sleeve when stacking a lot of work with a short deadline, because you said so. Value your skills and free time accordingly.',

    experience:
      'Explain every work experience you’ve had in detail – what were your obligations and tasks, what was your job title, which technologies and tools did you use etc. \nDon’t be shy on listing experience that isn’t connected to your current profession. Volunteering at a local community center, summer jobs, projects you were a part of, pro bono work you did to improve your skills and gain experience – everything counts. Plus, every experience serves as proof to your work ethics and adaptability. \nTip: If you have no experience and education to build up your profile, do some pro bono work or volunteer. You can help someone, and at the same time enrich your portfolio.',

    education:
      'Under education, list all the formal and non-formal education you have, with focus on the education that is relevant to your current title. For example, if you put Brainster Coding Academy as an education, list all the modules and projects you have worked on. Make sure to list your major and bachelor/master/doctor thesis subject under the university education.',

    activity:
      'Tip: Add connections from the same field of work as you; even people you don’t know personally. That way, recruiters have a bigger chance of coming across your profile.\nPut in the skills you are most experienced in, so you can be endorsed from other people.\nTip: Ask friends and coworkers to endorse you.',

    interests:
      'List all your relevant accomplishments. Don’t say you have a black belt in karate, but mention an award from a hackaton.',
  },

  laika: {
    email: 'Tip: Make sure you leave an email that you check regularly',

    socialMedia:
      'Link all your social media and portfolios you want companies and recruiters to be able to see (LinkedIn, Facebook, etc.)',

    whyLaika:
      'Be realistic when choosing the reason why you have a portfolio on Laika. This will help recruiters know whether you are suitable for the position on the long run.',

    industry:
      '*You can only pick 1 industry out of the given 9 (Software Engineering, Design, Marketing and Communication, Data Science, IT and Sysadmin, Sales and Business Development, HR and Recruitment, Project and Product Management, Customer Support).',

    expertise:
      '*You can pick up to 5 options in the Expertise field. Make sure your choices are realistic and they truly reflect the skills you are most confident in.',

    technologies:
      '*Choose up to 8 technologies, but make sure you really know their ins and outs. The matching algorithm connects you to companies whose job openings have precise technologies listed.',

    experience:
      'Work experience is not mandatory, but it helps companies know what kind of experience and in which industry you have.',

    salary:
      '*Select your desired salary. Don’t try to be too accessible, but please be realistic – make sure you desired salary is in accordance to your experience.',

    jobPlan: '*You can choose multiple choices for your desired job plan',
    locations: '*You can choose out of four locations, or multiple',
    jobType: '*You can choose between a job in office, remote, or both.',
    jobTitle: '*Let the companies know what is your current job title.',

    opportunity:
      '*Be honest about the motivation behind looking for a new opportunity. This will help us and the companies searching for new employees.',

    experienceLevel:
      '*Please be honest when selecting the level of your experience',

    education:
      'Education is also not mandatory, but it adds weight to your portfolio if you have any academic knowledge.',
  },
};

export const cvBuildForm = [
  {
    cvElements: {
      basicInfo: {
        name: '',
        title: '',
        goals: '',
      },

      image: imgLogo,

      contactInfo: {
        email: '',
        phone: '',
        location: '',
        twitter: '',
      },

      workExperienceArr: [
        {
          id: uuid(),
          title: '',
          workplace: '',
          startMonth: '',
          startYear: '',
          endMonth: '',
          endYear: '',
          present: false,
          location: '',
          description: '',
          achievements: '',
          responsibility: '',
        },
      ],

      educationArr: [
        {
          id: uuid(),
          study: '',
          institution: '',
          startMonth: '',
          startYear: '',
          endMonth: '',
          endYear: '',
          present: false,
          location: '',
          courses: '',
          projects: '',
        },
      ],

      skillsArr: ['New Skill', 'New Skill'],

      achievementsArr: [
        {
          id: uuid(),
          achievements: '',
          description: '',
        },
      ],

      languagesArr: [
        {
          id: uuid(),
          language: '',
          level: '',
        },
        {
          id: uuid(),
          language: '',
          level: '',
        },
      ],

      informalEducationArr: [
        'Artificial Intelligence Webinar',
        'Business Analytics Course',
      ],

      dataScienceSkillsArr: [
        {
          id: uuid(),
          skill: 'Python',
          level: 4,
        },
        {
          id: uuid(),
          skill: 'MySQL',
          level: 4,
        },
        {
          id: uuid(),
          skill: 'PHP',
          level: 3,
        },
        {
          id: uuid(),
          skill: 'R',
          level: 3,
        },
        {
          id: uuid(),
          skill: 'C',
          level: 3,
        },
      ],
    },
  },
];
